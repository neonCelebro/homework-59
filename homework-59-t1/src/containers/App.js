import React, { Component } from 'react';
import './App.css';
import AddFilmForm from '../components/AddFilmForm/AddFilmForm';
import MyFilms from '../components/MyFilms/MyFilms';

class App extends Component {
  state = {
    films: [],
    filmToWath: '',
  }

  newFilm = (e) => this.setState({filmToWath: e.target.value});

  changeFilm = (event, index) => {
    let films = [...this.state.films];
    let film = event.target.value;
    films[index] = film;
    this.setState({films})
  };

  addNewFilm = (e) => {
    e.preventDefault();
    const films = [...this.state.films];
    let filmToWath = this.state.filmToWath;
    films.push(filmToWath);
    filmToWath = '';
    this.setState({films, filmToWath});
  };

  remuveFilm = (index) =>{
    let films = [...this.state.films];
    films.splice(index, 1);
    this.setState({films})
  };

    componentWillMount(){
      if (localStorage['films'] !== undefined) {
        let films = JSON.parse(localStorage['films']) ;
        this.setState({films});
      }
    }
  render() {
    return (
      <div className="App">
        <AddFilmForm
          value={this.state.filmToWath}
          change={this.newFilm}
          submit={this.addNewFilm}
         />
        <MyFilms
          films={this.state.films}
          change={this.changeFilm}
          cliked={this.remuveFilm}/>
      </div>
    );
  }
}

export default App;
