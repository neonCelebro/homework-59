import React, { Component } from 'react';
import './MyFilms.css';
import Task from '../Task/Task';


class MyFilms extends Component {
  componentDidUpdate(){
    localStorage.setItem("films", JSON.stringify(this.props.films));
  }
  render(){
  if (this.props.films.length === 0) {
  return <div className="container">У Вас пока нет записей</div>
  }
  return (
    <div className="container">

      {this.props.films.map((film, index) =>{
        return <Task
          key={index}
          change={(event)=> this.props.change(event, index)}
          film={film}
          cliked={()=> this.props.cliked(index)}
               />
      })}
    </div>
  )
}
};

export default MyFilms;
