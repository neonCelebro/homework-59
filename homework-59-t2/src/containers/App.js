import React, { Component } from 'react';
import './App.css';
import Dask from '../components/Dask/Dask';
import Button from '../components/UI/Button/Button';
import Input from '../components/UI/Input/Input';
import Wrapper from '../components/UI/Wrapper/Wrapper';

class App extends Component {
  state = {
    jokes: [],
    amountJokes: 5,
    showWrap: false,
  }
  changeAmountJokes = (e) => {
    let amountJokes = e.target.value;
    this.setState({amountJokes});
  };

  getJoke = () =>{
    const URL_JOKE = 'https://api.chucknorris.io/jokes/random';
    return(
    fetch(URL_JOKE).then(response => {
      if (response.ok) {
        this.setState({showWrap:true});
        return response.json();
      };
    })
  )
  };

  getFiveJokes = () =>{
    const JOKES = [];
    for (var i = 0; i < this.state.amountJokes; i++) {
      JOKES.push(this.getJoke());
    }
    Promise.all(JOKES).then(values => {
      this.setState({showWrap:false})
      let jokes = values.map((joke) => joke.value);
      this.setState({jokes});
    });

  };

  componentDidMount() {
    this.getFiveJokes();
  };


  render() {
    return (
      <div className="App">
        <Input
          changed={this.changeAmountJokes}
          value={this.state.amountJokes}
        />
        <Button
          btnType='Purple'
          label="Get new Jokes"
          cliked={()=> this.getFiveJokes()}
        />
        <Dask
          jokes={this.state.jokes}
        />
        <Wrapper show={this.state.showWrap}/>
      </div>
    );
  }
};

export default App;
