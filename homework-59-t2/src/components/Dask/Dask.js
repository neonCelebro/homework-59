import React, { Component } from 'react';
import './Dask.css';


class Dask extends Component {

  render(){
  return (
    <div className="container">
      {this.props.jokes.map((joke, index) =>{
        return <p className='jokes' key={index}>{joke}</p>
      })}
    </div>
  )
}
};

export default Dask;
