import React from 'react';
import logo from './logo.svg';
import './Wrapper.css';

const Wrapper = props => (
  props.show ? <div className='Wrapper'
               >
    <img src={logo} className="App-logo" alt="logo" />
    <p className='textWrapper'>Please wait...</p></div> : null
);

export default Wrapper;
