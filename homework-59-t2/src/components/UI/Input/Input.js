import React from 'react';

const Input = props => (
      <input onChange={props.changed} value={props.value} type='number'/>
    );

export default Input;
