import React, {Component} from 'react';
import './Button.css';

class Button extends Component{

  shouldComponentUpdate(nextprops, nextState) {
    return false;
  };
    render(){
      return(
      <button className={['Button', this.props.btnType].join(' ')}
        onClick={this.props.cliked}>
        {this.props.label}
      </button>
    )
  }
  };

export default Button;
